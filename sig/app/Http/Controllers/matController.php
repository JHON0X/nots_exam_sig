<?php

namespace App\Http\Controllers;
use App\Model\Mmat;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class matController extends Controller
{
    //
    public function index(){
        $Mmat = Mmat::all();
        $data = $Mmat->toArray();

        $response = [
            'success' => true,
            'data' => $data,
            'message' => 'mat  successfully.'
        ];

        return response()->json($response, 200);
       // return Mmat::all();

    }
    
    public function show($id){

        $mat=Mmat::find($id);
        return $mat;
    }
    public function update(Request $request, $id){

        $mat=Mmat::find($id);
        $mat->name_mat = $request->name_mat;
        $mat->time_mat = $request->time_mat;
        $mat->save();
        return redirect("matController".$id);
    }
    public function destroy($id){
        
        $mat = Mmat::find($id);
        $mat->delete();
        return 'deleted';
    }
    
    public function store(Request $request){
        $mat= new Mmat;
        $mat->nam_mat=$request->nam_mat;
        //$mat->time_mat=$request->time_mat;
        $mat->save();
        
        return 'saved';
    }

    

}
