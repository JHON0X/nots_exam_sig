<?php

namespace App\Http\Controllers;

use App\Model\Mteacher;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class teacherController extends Controller
{
    //
    public function index(){
        $teacher = Mteacher::all();
        $data = $teacher->toArray();

        $response = [
            'success' => true,
            'data' => $data,
            'message' => 'mat  successfully.'
        ];

        return response()->json($response, 200);
       // return Mmat::all();

    }
    
    public function show($id){

        $teacher=Mteacher::find($id);
        return $teacher;
    }
    public function update(Request $request, $id){

        $teacher=Mteacher::find($id);
        $teacher->fname_te = $request->fname_te;
        $teacher->lname_te = $request->lname_te;
        $teacher->email_te = $request->email_te;
        $teacher->save();
        return redirect("matController".$id);
    }
    public function destroy($id){
        
        $teacher = Mteacher::find($id);
        $teacher->delete();
        return 'deleted';
    }
    
    public function store(Request $request){
        $teacher= new Mteacher;
        $teacher->fname_te = $request->fname_te;
        $teacher->lname_te = $request->lname_te;
        $teacher->email_te = $request->email_te;
        $teacher->save();
        
        return 'saved';
    }
}
