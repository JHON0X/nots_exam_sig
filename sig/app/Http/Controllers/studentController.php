<?php

namespace App\Http\Controllers;

use App\Model\Mstudent;

use Illuminate\Http\Request;

class studentController extends Controller
{
    //
    public function index(){
        $Mstudent = Mstudent::all();
        $data = $Mstudent->toArray();

        $response = [
            'success' => true,
            'data' => $data,
            'message' => 'mat  successfully.'
        ];

        return response()->json($response, 200);
       // return Mmat::all();

    }
    
    public function show($id){

        $student=Mstudent::find($id);
        return $student;
    }
    public function update(Request $request, $id){

        $student=Mstudent::find($id);
        $student->fname_s = $request->fname_s;
        $student->lname_s = $request->lname_s;
        $student->email_s = $request->email_s;
        $student->save();
        return redirect("matController".$id);
    }
    public function destroy($id){
        
        $student = Mstudent::find($id);
        $student->delete();
        return 'deleted';
    }
    
    public function store(Request $request){
        $student= new Mstudent;
        $student->fname_s = $request->fname_s;
        $student->lname_s = $request->lname_s;
        $student->email_s = $request->email_s;
        $student->save();
        
        return 'saved';
    }
}
