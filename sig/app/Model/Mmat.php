<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Mmat extends Model
{
    //
    protected $table= 'mat';
    protected $primarykey='id_mat';

    public $timestamps= true;
    const CREATED_AT = 'date_create';
    const UPDATED_AT = 'date_update';
    
    protected $fillable=[
        'nam_mat',
        'time_mat',
        'date_create',
        'date_update',
    ];
}
