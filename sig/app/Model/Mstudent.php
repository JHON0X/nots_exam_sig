<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Mstudent extends Model
{
    //
    protected $table= 'student';
    protected $primarykey='id_student';

    public $timestamps= true;
    const CREATED_AT = 'date_create';
    const UPDATED_AT = 'date_update';
    
    protected $fillable=[
        'fname_s',
        'lname_s',
        'email_s',
        'date_create',
        'date_update',

    ];
}
