<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Mteacher extends Model
{
    //
    protected $table= 'teacher';
    protected $primarykey='id_te';

    public $timestamps= true;
    const CREATED_AT = 'date_create';
    const UPDATED_AT = 'date_update';
    
    protected $fillable=[
        'fname_te',
        'lname_te',
        'email_te',
        'date_create',
        'date_update',

    ];
}
