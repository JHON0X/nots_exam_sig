-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 06-06-2019 a las 05:06:40
-- Versión del servidor: 10.1.38-MariaDB
-- Versión de PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sis_not`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mat`
--

CREATE TABLE `mat` (
  `id_mat` bigint(20) UNSIGNED NOT NULL,
  `nam_mat` text COLLATE utf8mb4_unicode_ci,
  `time_mat` datetime DEFAULT NULL,
  `date_create` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `mat`
--

INSERT INTO `mat` (`id_mat`, `nam_mat`, `time_mat`, `date_create`, `date_update`) VALUES
(1, 'biologia', NULL, '2019-06-06 01:15:49', '2019-06-06 01:15:49'),
(2, 'dkaskdka', NULL, '2019-06-06 01:16:17', '2019-06-06 01:16:17'),
(3, 'programacion I', NULL, '2019-06-06 02:37:30', '2019-06-06 02:37:30'),
(4, 'Base de Datos', NULL, '2019-06-06 02:40:28', '2019-06-06 02:40:28'),
(5, 'calculo I', NULL, '2019-06-06 02:58:17', '2019-06-06 02:58:17');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(33, '2014_10_12_000000_create_users_table', 1),
(34, '2014_10_12_100000_create_password_resets_table', 1),
(35, '2019_06_05_232738_create_student_table', 1),
(36, '2019_06_05_232944_create_mat_table', 1),
(37, '2019_06_05_232959_create_teacher_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `student`
--

CREATE TABLE `student` (
  `id_student` bigint(20) UNSIGNED NOT NULL,
  `fname_s` text COLLATE utf8mb4_unicode_ci,
  `lname_s` text COLLATE utf8mb4_unicode_ci,
  `email_s` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_create` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `student`
--

INSERT INTO `student` (`id_student`, `fname_s`, `lname_s`, `email_s`, `date_create`, `date_update`) VALUES
(1, 'dkaskdka', 'fghjbh', 'fyugih', '2019-06-06 01:23:47', '2019-06-06 01:23:47'),
(2, 'hola', 'fghjbh', 'fyugih', '2019-06-06 01:28:10', '2019-06-06 01:28:10'),
(3, 'tfyguh', 'fyguh', 'fpioghf', '2019-06-06 02:28:37', '2019-06-06 02:28:37'),
(4, 'jhonn', 'solares', 'jesolares-es@udabol.edu.bo', '2019-06-06 02:36:36', '2019-06-06 02:36:36'),
(5, 'jorge', 'cervantez', 'jmcervantez-es@udabol.edu.bo', '2019-06-06 02:49:23', '2019-06-06 02:49:23'),
(6, 'joker', 'wason', 'joker@udabol.edu.bo', '2019-06-06 02:59:09', '2019-06-06 02:59:09');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `teacher`
--

CREATE TABLE `teacher` (
  `id_te` bigint(20) UNSIGNED NOT NULL,
  `fname_te` text COLLATE utf8mb4_unicode_ci,
  `lname_te` text COLLATE utf8mb4_unicode_ci,
  `email_te` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_create` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `teacher`
--

INSERT INTO `teacher` (`id_te`, `fname_te`, `lname_te`, `email_te`, `date_create`, `date_update`) VALUES
(1, NULL, NULL, NULL, '2019-06-06 01:28:51', '2019-06-06 01:28:51'),
(2, 'hola', 'fghjbh', 'fyugih', '2019-06-06 01:29:16', '2019-06-06 01:29:16'),
(3, 'hola', 'fghjbh', 'fyugih', '2019-06-06 01:30:00', '2019-06-06 01:30:00'),
(4, 'pepe', 'metalo', 'pep_metalo@gmail.com', '2019-06-06 02:38:34', '2019-06-06 02:38:34'),
(5, 'jorge', 'cervantez', 'jmcervantez-es@udabol.edu.bo', '2019-06-06 02:57:32', '2019-06-06 02:57:32'),
(6, 'ivan', 'felix', 'felix_ivan@udabol.edu.bo', '2019-06-06 03:00:02', '2019-06-06 03:00:02');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `mat`
--
ALTER TABLE `mat`
  ADD PRIMARY KEY (`id_mat`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id_student`);

--
-- Indices de la tabla `teacher`
--
ALTER TABLE `teacher`
  ADD PRIMARY KEY (`id_te`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `mat`
--
ALTER TABLE `mat`
  MODIFY `id_mat` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT de la tabla `student`
--
ALTER TABLE `student`
  MODIFY `id_student` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `teacher`
--
ALTER TABLE `teacher`
  MODIFY `id_te` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
